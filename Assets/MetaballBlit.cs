﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[ImageEffectAllowedInSceneView]
public class MetaballBlit : MonoBehaviour
{
    [Range(0,1f)]
    public float threshold = .5f;
    [Range(0, 1f)]
    public float smoothThreshold = .5f;

    public Material material;

    public void Update()
    {
        material.SetFloat("_Threshold", threshold);
        material.SetFloat("_SThreshold", smoothThreshold);
    }

    public void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        Graphics.Blit(src, dest, material);
    }
}
